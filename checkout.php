<?php
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    die();
}
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: *");
$json = file_get_contents('php://input');
$input= json_decode( $json,true ); 

$to = 'sina.pcs@gmail.com';
$subject = 'New Order';

$str = "<table><tbody>";
foreach ($input as $key => $val) {
    $str .= "<tr>";
    $str .= "<td>$key</td>";
    $str .= "<td>";
    if (is_array($val)) {
        foreach ($val as $key2=> $val2){
            $str .= $val2['Title'].'<br>';
        }
    } else {
        $str .= "<strong>$val</strong>";
    }
    $str .= "</td></tr>";
}
$str .= "</tbody></table>";

$htmlContent = '
    <html>
    <head>
        <title>XEPOS</title>
    </head>
    <body>
        '.$str.'
    </body>
    </html>';

// Set content-type header for sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// Additional headers
$headers .= 'From: XEPOS <website.leads@xepos.co.uk>' . "\r\n";

// Send email
if(mail($to,$subject,$htmlContent,$headers)):
    echo $str;
else:
   $errorMsg = 'Email sending fail.';
    echo $errorMsg;
endif;

$htmlContent = '
    <html>
    <head>
        <title>XEPOS</title>
    </head>
    <body>
    <div style="width:300px; margin:auto;">
        <div>
            We have received your message and would like to thank you for contacting us. If your inquiry is urgent, please use the telephone number listed below, to talk one of our staff members. Otherwise, we will reply by email shortly.
        </div>
        <div style="width: 125px;
        text-align: center;
        padding: 10px;
        margin: 20px auto;
        border-radius: 5px;">
            <img src="http://xepos.co.uk/assets/img/logo.svg" alt="Xepos-Logo" title="Xepos-Logo" style="display:block" width="120" height="30" />
        </div>
        <div style="font-size: 17px;
        text-align: center;">
            <strong>Tel:</strong> 0345 0345 930
        </div>
    </div>
    </body>
    </html>
';
//mail($email,$subject,$htmlContent,$headers);
?>