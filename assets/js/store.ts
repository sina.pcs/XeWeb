let countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua &amp; Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Cape Verde","Cayman Islands","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cruise Ship","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kuwait","Kyrgyz Republic","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Mauritania","Mauritius","Mexico","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Namibia","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Pakistan","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre &amp; Miquelon","Samoa","San Marino","Satellite","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","South Africa","South Korea","Spain","Sri Lanka","St Kitts &amp; Nevis","St Lucia","St Vincent","St. Lucia","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks &amp; Caicos","Uganda","Ukraine","United Arab Emirates","United Kingdom","Uruguay","Uzbekistan","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];

angular.module("store", ["ngRoute","ngStorage"])
    .config(function($routeProvider,$locationProvider) {
        $locationProvider.hashPrefix('');
        $routeProvider
            .when("/", {
                templateUrl : "main.htm"
            })
            .when("/product/:id", {
                templateUrl : "product.htm"
            })
            .otherwise({
                redirectTo: '/'
            });
    })
    .controller("StoreController", function($localStorage) {
        this.products = products;
        this.addToBasket = (product) => {
            let productsInOrder : any = $localStorage.productsInOrder ? $localStorage.productsInOrder : [];
            let productIndexInOrder = productsInOrder.findIndex((p)=>{return p.Id == this.id});
            if(productIndexInOrder != -1){
                productsInOrder[productIndexInOrder].Quantity = parseInt(productsInOrder[productIndexInOrder].Quantity) + 1;
            }
            else{
                product.Quantity = 1;
                productsInOrder.push(product);
            }
            $localStorage.productsInOrder = productsInOrder;
            $localStorage.productsInBasket = $localStorage.productsInBasket?$localStorage.productsInBasket +1: 1;
            $(".badge").html($localStorage.productsInBasket);

            //this.showPopup = true;
            $('.modal .modal-content').removeClass('fadeOutUp').addClass('fadeInDown');
            $('#checkModal').addClass('active');
        };
    })
    .controller("ProductController", function($routeParams,$scope,$localStorage) {
        this.id = $routeParams.id;
        this.showPopup = false;
        this.product = products.find((p)=>{return p.Id == this.id});
        this.addToBasket = (product) => {
            let productsInOrder : any = $localStorage.productsInOrder ? $localStorage.productsInOrder : [];
            let productIndexInOrder = productsInOrder.findIndex((p)=>{return p.Id == this.id});
            if(productIndexInOrder != -1){
                productsInOrder[productIndexInOrder].Quantity = parseInt(productsInOrder[productIndexInOrder].Quantity) + 1;
            }
            else{
                product.Quantity = 1;
                productsInOrder.push(product);
            }
            $localStorage.productsInOrder = productsInOrder;
            $localStorage.productsInBasket = $localStorage.productsInBasket?$localStorage.productsInBasket +1: 1;
            $(".badge").html($localStorage.productsInBasket);

            //this.showPopup = true;
            $('.modal .modal-content').removeClass('fadeOutUp').addClass('fadeInDown');
            $('#checkModal').addClass('active');
        };
    });

angular.module("checkout", ['ngStorage'])
    .controller("CheckoutController", function($localStorage,$http) {
        this.countries = countries;
        this.checkoutStatus = 'form';
        this.loading = false;
        this.businessTypes = ["Retail","Hospitality","Takeaway","Health & Beauty","Convenience Store"]
        this.productsInOrder = $localStorage.productsInOrder ? $localStorage.productsInOrder : [];
        this.subTotal = 0;
        this.VAT = 10;
        this.buyer = {
            email:'',
            tel:'',
            firstName:'',
            lastName:'',
            address:'',
            city:'',
            country:'',
            postCode:'',
            shipToSameAddress:false,
            createAccount:false,
            typeOfBusiness:'Retail',
            paymentMethod:'',
            cardNumber:'',
            nameOnCard:'',
            mmyy:'',
            cvv:''
        };

        for(let product of this.productsInOrder){
            this.subTotal += parseInt(product.Quantity) * parseFloat(product.Price);
        };

        this.updateProductsInOrder = (productId,Quantity) => {
            let productsInOrder : any = $localStorage.productsInOrder ? $localStorage.productsInOrder : [];
            if(Quantity==""){
                let product = productsInOrder.find((p)=>{return p.Id == productId});
                product.Quantity = 0;
            }
            let productsInBasket = 0;
            this.subTotal = 0;
            for(let product of productsInOrder)
            {
                productsInBasket = productsInBasket + parseInt(product.Quantity);
                this.subTotal += parseInt(product.Quantity) * parseFloat(product.Price);
            }
            $localStorage.productsInOrder = this.productsInOrder;
            $localStorage.productsInBasket = productsInBasket;
            $(".badge").html(productsInBasket.toString());
        };

        this.deleteProductInOrder = (productId) => {
            let productsInOrder : any = $localStorage.productsInOrder ? $localStorage.productsInOrder : [];
            let productIndex = productsInOrder.findIndex((p)=>{return p.Id == productId});
            let productQuantity = productsInOrder[productIndex].Quantity;
            let productPrice= productsInOrder[productIndex].Price;
            this.subTotal -= parseInt(productQuantity) * parseFloat(productPrice);
            productsInOrder.splice(productIndex,1);
            $localStorage.productsInOrder = this.productsInOrder;
            $localStorage.productsInBasket = $localStorage.productsInBasket - productQuantity;
            $(".badge").html($localStorage.productsInBasket.toString());
        };

        this.completeOrder=(event)=>{
            event.preventDefault();
            this.loading = true;
            let objectToSend = this.buyer;
            objectToSend.shipToSameAddress = objectToSend.shipToSameAddress?'Yes':'No';
            objectToSend.createAccount = objectToSend.createAccount?'Yes':'No';

            let datetime = new Date().toUTCString();

            let buyer = '<div>' +
                '<h3>Customer Information</h3>' +
                '<strong>'+datetime+'</strong><br>' +
                '<table>';
            for(let key in objectToSend){
                buyer += '<tr>' +
                                '<td>'+key+'</td>' +
                                '<td>'+objectToSend[key]+'</td>'+
                            '</tr>'
            }


            buyer += '</table>';

            let products = '<div>' +
                '<table>' +
                '<tr>' +
                    '<th>Name</th>' +
                    '<th>Quantity</th>' +
                    '<th>Price</th>' +
                '</tr>';

            for(let product of this.productsInOrder){
                products += '<tr>' +
                    '<td>'+product.Title+'</td>' +
                    '<td>'+product.Quantity+'</td>'+
                    '<td>'+product.Price+'</td>'+
                    '</tr>'
            }

            products += '</table>';

            let content = '<div>' +
                buyer + '<h3>Order Details</h3>' + products +
                '</div>';

            let mailToUser = '<html>' +
            '<head>' +
            '<title>XEPOS</title>' +
            '</head>' +
                '<body>' +
                    '<div style="width:300px; margin:auto;">' +
                        '<div>' +
                            'We have received your message and would like to thank you for contacting us. If your inquiry is urgent, please use the telephone number listed below, to talk one of our staff members. Otherwise, we will reply by email shortly.' +
                        '</div>' +
                    '<div style="width: 125px; text-align: center; padding: 10px; margin: 20px auto; border-radius: 5px;">' +
                        '<img src="http://xepos.co.uk/assets/img/logo.svg" alt="Xepos-Logo" title="Xepos-Logo" style="display:block" width="120" height="30" />' +
                    '</div>' +
                    '<div style="font-size: 17px; text-align: center;">' +
                        '<strong>Tel:</strong> 0345 0345 930' +
                    '</div>' +
                    '</div>' +
                '</body>' +
            '</html>';


            $http.post('http://xepos.co.uk/mail.php',
                {
                    content : content,
                    to : 'sina@xepos.co.uk',
                    subject : 'New Order (' + datetime +')',
                    mailToUser: mailToUser,
                    responseToUser : mailToUser
                }
            ).then((result) => {
                $localStorage.productsInOrder = [];
                $localStorage.productsInBasket = 0;
                $(".badge").html('0');
                this.checkoutStatus = 'done';
                this.loading = false;
            })
                .catch((error)=>{
                    this.checkoutStatus = 'fail';
                    this.loading = false;
            })
        }
    })
    .directive('selectOnClick', ['$window', function ($window) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.on('click', function () {
                    if (!$window.getSelection().toString()) {
                        // Required for mobile Safari
                        this.setSelectionRange(0, this.value.length)
                    }
                });
            }
        };
    }])
;