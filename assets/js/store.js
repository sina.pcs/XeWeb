var countries = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua &amp; Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia &amp; Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Cape Verde", "Cayman Islands", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cruise Ship", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kuwait", "Kyrgyz Republic", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Mauritania", "Mauritius", "Mexico", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre &amp; Miquelon", "Samoa", "San Marino", "Satellite", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "South Africa", "South Korea", "Spain", "Sri Lanka", "St Kitts &amp; Nevis", "St Lucia", "St Vincent", "St. Lucia", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad &amp; Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks &amp; Caicos", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "Uzbekistan", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];
angular.module("store", ["ngRoute", "ngStorage"])
    .config(function ($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('');
    $routeProvider
        .when("/", {
        templateUrl: "main.htm"
    })
        .when("/product/:id", {
        templateUrl: "product.htm"
    })
        .otherwise({
        redirectTo: '/'
    });
})
    .controller("StoreController", function ($localStorage) {
    var _this = this;
    this.products = products;
    this.addToBasket = function (product) {
        var productsInOrder = $localStorage.productsInOrder ? $localStorage.productsInOrder : [];
        var productIndexInOrder = productsInOrder.findIndex(function (p) { return p.Id == _this.id; });
        if (productIndexInOrder != -1) {
            productsInOrder[productIndexInOrder].Quantity = parseInt(productsInOrder[productIndexInOrder].Quantity) + 1;
        }
        else {
            product.Quantity = 1;
            productsInOrder.push(product);
        }
        $localStorage.productsInOrder = productsInOrder;
        $localStorage.productsInBasket = $localStorage.productsInBasket ? $localStorage.productsInBasket + 1 : 1;
        $(".badge").html($localStorage.productsInBasket);
        //this.showPopup = true;
        $('.modal .modal-content').removeClass('fadeOutUp').addClass('fadeInDown');
        $('#checkModal').addClass('active');
    };
})
    .controller("ProductController", function ($routeParams, $scope, $localStorage) {
    var _this = this;
    this.id = $routeParams.id;
    this.showPopup = false;
    this.product = products.find(function (p) { return p.Id == _this.id; });
    this.addToBasket = function (product) {
        var productsInOrder = $localStorage.productsInOrder ? $localStorage.productsInOrder : [];
        var productIndexInOrder = productsInOrder.findIndex(function (p) { return p.Id == _this.id; });
        if (productIndexInOrder != -1) {
            productsInOrder[productIndexInOrder].Quantity = parseInt(productsInOrder[productIndexInOrder].Quantity) + 1;
        }
        else {
            product.Quantity = 1;
            productsInOrder.push(product);
        }
        $localStorage.productsInOrder = productsInOrder;
        $localStorage.productsInBasket = $localStorage.productsInBasket ? $localStorage.productsInBasket + 1 : 1;
        $(".badge").html($localStorage.productsInBasket);
        //this.showPopup = true;
        $('.modal .modal-content').removeClass('fadeOutUp').addClass('fadeInDown');
        $('#checkModal').addClass('active');
    };
});
angular.module("checkout", ['ngStorage'])
    .controller("CheckoutController", function ($localStorage, $http) {
    var _this = this;
    this.countries = countries;
    this.checkoutStatus = 'form';
    this.loading = false;
    this.businessTypes = ["Retail", "Hospitality", "Takeaway", "Health & Beauty", "Convenience Store"];
    this.productsInOrder = $localStorage.productsInOrder ? $localStorage.productsInOrder : [];
    this.subTotal = 0;
    this.VAT = 10;
    this.buyer = {
        email: '',
        tel: '',
        firstName: '',
        lastName: '',
        address: '',
        city: '',
        country: '',
        postCode: '',
        shipToSameAddress: false,
        createAccount: false,
        typeOfBusiness: 'Retail',
        paymentMethod: '',
        cardNumber: '',
        nameOnCard: '',
        mmyy: '',
        cvv: ''
    };
    for (var _i = 0, _a = this.productsInOrder; _i < _a.length; _i++) {
        var product = _a[_i];
        this.subTotal += parseInt(product.Quantity) * parseFloat(product.Price);
    }
    ;
    this.updateProductsInOrder = function (productId, Quantity) {
        var productsInOrder = $localStorage.productsInOrder ? $localStorage.productsInOrder : [];
        if (Quantity == "") {
            var product = productsInOrder.find(function (p) { return p.Id == productId; });
            product.Quantity = 0;
        }
        var productsInBasket = 0;
        _this.subTotal = 0;
        for (var _i = 0, productsInOrder_1 = productsInOrder; _i < productsInOrder_1.length; _i++) {
            var product = productsInOrder_1[_i];
            productsInBasket = productsInBasket + parseInt(product.Quantity);
            _this.subTotal += parseInt(product.Quantity) * parseFloat(product.Price);
        }
        $localStorage.productsInOrder = _this.productsInOrder;
        $localStorage.productsInBasket = productsInBasket;
        $(".badge").html(productsInBasket.toString());
    };
    this.deleteProductInOrder = function (productId) {
        var productsInOrder = $localStorage.productsInOrder ? $localStorage.productsInOrder : [];
        var productIndex = productsInOrder.findIndex(function (p) { return p.Id == productId; });
        var productQuantity = productsInOrder[productIndex].Quantity;
        var productPrice = productsInOrder[productIndex].Price;
        _this.subTotal -= parseInt(productQuantity) * parseFloat(productPrice);
        productsInOrder.splice(productIndex, 1);
        $localStorage.productsInOrder = _this.productsInOrder;
        $localStorage.productsInBasket = $localStorage.productsInBasket - productQuantity;
        $(".badge").html($localStorage.productsInBasket.toString());
    };
    this.completeOrder = function (event) {
        event.preventDefault();
        _this.loading = true;
        var objectToSend = _this.buyer;
        objectToSend.shipToSameAddress = objectToSend.shipToSameAddress ? 'Yes' : 'No';
        objectToSend.createAccount = objectToSend.createAccount ? 'Yes' : 'No';
        var datetime = new Date().toUTCString();
        var buyer = '<div>' +
            '<h3>Customer Information</h3>' +
            '<strong>' + datetime + '</strong><br>' +
            '<table>';
        for (var key in objectToSend) {
            buyer += '<tr>' +
                '<td>' + key + '</td>' +
                '<td>' + objectToSend[key] + '</td>' +
                '</tr>';
        }
        buyer += '</table>';
        var products = '<div>' +
            '<table>' +
            '<tr>' +
            '<th>Name</th>' +
            '<th>Quantity</th>' +
            '<th>Price</th>' +
            '</tr>';
        for (var _i = 0, _a = _this.productsInOrder; _i < _a.length; _i++) {
            var product = _a[_i];
            products += '<tr>' +
                '<td>' + product.Title + '</td>' +
                '<td>' + product.Quantity + '</td>' +
                '<td>' + product.Price + '</td>' +
                '</tr>';
        }
        products += '</table>';
        var content = '<div>' +
            buyer + '<h3>Order Details</h3>' + products +
            '</div>';
        var mailToUser = '<html>' +
            '<head>' +
            '<title>XEPOS</title>' +
            '</head>' +
            '<body>' +
            '<div style="width:300px; margin:auto;">' +
            '<div>' +
            'We have received your message and would like to thank you for contacting us. If your inquiry is urgent, please use the telephone number listed below, to talk one of our staff members. Otherwise, we will reply by email shortly.' +
            '</div>' +
            '<div style="width: 125px; text-align: center; padding: 10px; margin: 20px auto; border-radius: 5px;">' +
            '<img src="http://xepos.co.uk/assets/img/logo.svg" alt="Xepos-Logo" title="Xepos-Logo" style="display:block" width="120" height="30" />' +
            '</div>' +
            '<div style="font-size: 17px; text-align: center;">' +
            '<strong>Tel:</strong> 0345 0345 930' +
            '</div>' +
            '</div>' +
            '</body>' +
            '</html>';
        $http.post('http://xepos.co.uk/mail.php', {
            content: content,
            to: 'sina@xepos.co.uk',
            subject: 'New Order (' + datetime + ')',
            mailToUser: mailToUser,
            responseToUser: mailToUser
        }).then(function (result) {
            $localStorage.productsInOrder = [];
            $localStorage.productsInBasket = 0;
            $(".badge").html('0');
            _this.checkoutStatus = 'done';
            _this.loading = false;
        })["catch"](function (error) {
            _this.checkoutStatus = 'fail';
            _this.loading = false;
        });
    };
})
    .directive('selectOnClick', ['$window', function ($window) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.on('click', function () {
                    if (!$window.getSelection().toString()) {
                        // Required for mobile Safari
                        this.setSelectionRange(0, this.value.length);
                    }
                });
            }
        };
    }]);
//# sourceMappingURL=store.js.map