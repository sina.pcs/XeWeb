var products = [
    { Type: 'Clover', products: [
            {
                Id: '01',
                Title: 'Clover Mini',
                Price: '15.50',
                PriceToShow: '£15.50 Per Month',
                Details: "Lorem Ipsum",
                Images: ['assets/img/Clover-Mini.png'],
                tags: 'clover',
                link: 'clover-mini'
            }
        ]
    },
    { Type: 'System', products: [
            {
                Id: '11',
                Title: 'Retail Complete System',
                Price: '899.99',
                PriceToShow: '£899.99 Buy Out',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'retail',
                link: 'retail/system.html'
            },
            {
                Id: '12',
                Title: 'Hospitality Complete System',
                Price: '899.99',
                PriceToShow: '£899.99 Buy Out',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'hospitality',
                link: 'hospitality/system.html'
            },
            {
                Id: '13',
                Title: 'Health & Beauty Complete System',
                Price: '899.99',
                PriceToShow: '£899.99 Buy Out',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'health',
                link: 'health-and-beauty/system.html'
            },
            {
                Id: '14',
                Title: 'Convenience Complete System',
                Price: '899.99',
                PriceToShow: '£899.99 Buy Out',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'health',
                link: 'health-and-beauty/system.html'
            },
            {
                Id: '15',
                Title: 'Takeaways Complete System',
                Price: '899.99',
                PriceToShow: '£899.99 Buy Out',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'health',
                link: 'health-and-beauty/system.html'
            },
            {
                Id: '16',
                Title: 'Retail Complete System',
                Price: '100.00',
                PriceToShow: '£100.00 Per Month',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'retail',
                link: 'retail/system.html'
            },
            {
                Id: '17',
                Title: 'Hospitality Complete System',
                Price: '100.00',
                PriceToShow: '£100.00 Per Month',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'hospitality',
                link: 'hospitality/system.html'
            },
            {
                Id: '18',
                Title: 'Health & Beauty Complete System',
                Price: '100.00',
                PriceToShow: '£100.00 Per Month',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'health',
                link: 'health-and-beauty/system.html'
            },
            {
                Id: '19',
                Title: 'Convenience Complete System',
                Price: '100.00',
                PriceToShow: '£100.00 Per Month',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'health',
                link: 'health-and-beauty/system.html'
            },
            {
                Id: '20',
                Title: 'Takeaways Complete System',
                Price: '100.00',
                PriceToShow: '£100.00 Per Month',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'health',
                link: 'health-and-beauty/system.html'
            }
        ]
    },
    { Type: 'Software', products: [
            {
                Id: '21',
                Title: 'Retail Software Only',
                Price: '899.99',
                PriceToShow: '£899.99 Buy Out',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'retail',
                link: 'retail/software.html'
            },
            {
                Id: '22',
                Title: 'Hospitality Software Only',
                Price: '899.99',
                PriceToShow: '£899.99 Buy Out',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'hospitality',
                link: 'hospitality/software.html'
            },
            {
                Id: '23',
                Title: 'Health & Beauty Software Only',
                Price: '899.99',
                PriceToShow: '£899.99 Buy Out',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'health',
                link: 'health-and-beauty/software.html'
            },
            {
                Id: '24',
                Title: 'Convenience Software Only',
                Price: '899.99',
                PriceToShow: '£899.99 Buy Out',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'health',
                link: 'convenience/software.html'
            },
            {
                Id: '25',
                Title: 'Takeaways Software Only',
                Price: '899.99',
                PriceToShow: '£899.99 Buy Out',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'health',
                link: 'takeaway/software.html'
            },
            {
                Id: '26',
                Title: 'Retail Software Only',
                Price: '100.00',
                PriceToShow: '£100.00 Per Month',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'retail',
                link: 'retail/software.html'
            },
            {
                Id: '27',
                Title: 'Hospitality Software Only',
                Price: '100.00',
                PriceToShow: '£100.00 Per Month',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'hospitality',
                link: 'hospitality/software.html'
            },
            {
                Id: '28',
                Title: 'Health & Beauty Software Only',
                Price: '100.00',
                PriceToShow: '£100.00 Per Month',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'health',
                link: 'health-and-beauty/software.html'
            },
            {
                Id: '29',
                Title: 'Convenience Software Only',
                Price: '100.00',
                PriceToShow: '£100.00 Per Month',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'health',
                link: 'convenience/software.html'
            },
            {
                Id: '30',
                Title: 'Takeaways Software Only',
                Price: '100.00',
                PriceToShow: '£100.00 Per Month',
                Details: "Lorem Ipsum",
                Images: ['assets/img/complete-systems.png'],
                tags: 'health',
                link: 'takeaway/software.html'
            }
        ] },
    { Type: 'Tablet', products: [
            {
                Id: '31',
                Title: 'Retail Tablet System',
                Price: '899.99',
                PriceToShow: '£899.99 Buy Out',
                Details: "Lorem Ipsum",
                Images: ['assets/img/software-only.png'],
                tags: 'retail',
                link: 'retail/system.html'
            },
            {
                Id: '32',
                Title: 'Hospitality Tablet System',
                Price: '899.99',
                PriceToShow: '£899.99 Buy Out',
                Details: "Lorem Ipsum",
                Images: ['assets/img/software-only.png'],
                tags: 'hospitality',
                link: 'hospitality/system.html'
            },
            {
                Id: '33',
                Title: 'Health & Beauty Tablet System',
                Price: '899.99',
                PriceToShow: '£899.99 Buy Out',
                Details: "Lorem Ipsum",
                Images: ['assets/img/software-only.png'],
                tags: 'health',
                link: 'health-and-beauty/system.html'
            },
            {
                Id: '34',
                Title: 'Convenience Tablet System',
                Price: '899.99',
                PriceToShow: '£899.99 Buy Out',
                Details: "Lorem Ipsum",
                Images: ['assets/img/software-only.png'],
                tags: 'health',
                link: 'convenience/system.html'
            },
            {
                Id: '35',
                Title: 'Takeaways Tablet System',
                Price: '899.99',
                PriceToShow: '£899.99 Buy Out',
                Details: "Lorem Ipsum",
                Images: ['assets/img/software-only.png'],
                tags: 'health',
                link: 'takeaway/system.html'
            },
            {
                Id: '36',
                Title: 'Retail Tablet System',
                Price: '100.00',
                PriceToShow: '£100.00 Per Month',
                Details: "Lorem Ipsum",
                Images: ['assets/img/software-only.png'],
                tags: 'retail',
                link: 'retail/system.html'
            },
            {
                Id: '37',
                Title: 'Hospitality Tablet System',
                Price: '100.00',
                PriceToShow: '£100.00 Per Month',
                Details: "Lorem Ipsum",
                Images: ['assets/img/software-only.png'],
                tags: 'hospitality',
                link: 'hospitality/system.html'
            },
            {
                Id: '38',
                Title: 'Health & Beauty Tablet System',
                Price: '100.00',
                PriceToShow: '£100.00 Per Month',
                Details: "Lorem Ipsum",
                Images: ['assets/img/software-only.png'],
                tags: 'health',
                link: 'health-and-beauty/system.html'
            },
            {
                Id: '39',
                Title: 'Convenience Tablet System',
                Price: '100.00',
                PriceToShow: '£100.00 Per Month',
                Details: "Lorem Ipsum",
                Images: ['assets/img/software-only.png'],
                tags: 'health',
                link: 'convenience/system.html'
            },
            {
                Id: '40',
                Title: 'Takeaways Tablet System',
                Price: '100.00',
                PriceToShow: '£100.00 Per Month',
                Details: "Lorem Ipsum",
                Images: ['assets/img/software-only.png'],
                tags: 'health',
                link: 'takeaway/system.html'
            }
        ] },
];
$(document).ready(function () {
    $.validate({
        lang: 'en',
        modules: 'sweden',
        borderColorOnError: '#ea564d'
    });
    $(".se-pre-con").fadeOut("slow");
    setHeaderHeight();
    var productsInBasket = localStorage.getItem("ngStorage-productsInBasket") ? localStorage.getItem("ngStorage-productsInBasket") : 0;
    $(".badge").html(productsInBasket.toString());
    $('.month-switch input').change(function () {
        if ($(this).is(":checked")) {
            $('.month-switch input').prop("checked", true);
            $('.pricing .prices .left .price').html('£250 <span>one-off<br>save 30%</span>');
            $('.pricing .prices .center .price').html('£899 <span>one-off<br>save 30%</span>');
            $('.pricing .prices .right .price').html('£700 <span>one-off<br>save 30%</span>');
            $('.pricing .prices .try-software-button').each(function (i, e) {
                var modalText = $(this).data('modal');
                modalText = modalText.replace('Per month', 'one-off');
                $(this).data('modal', modalText);
            });
            $('.footer-price-container .price').html('£100');
            $('.footer-price-container .try-software-button').each(function (i, e) {
                var modalText = $(this).data('modal');
                modalText = modalText.replace('Buy Out', 'Per Month');
                $(this).data('modal', modalText);
            });
            $('.price-container #price').html('£100');
            $('.price-container #software-price').html('£30');
            $('.try-software-button.buy-now-button').data('modal', 'Buy Now - Per Month');
        }
        else {
            $('.month-switch input').prop("checked", false);
            $('.pricing .prices .left .price').html('£30 <span>per<br>month</span>');
            $('.pricing .prices .center .price').html('£100 <span>per<br>month</span>');
            $('.pricing .prices .right .price').html('£85 <span>per<br>month</span>');
            $('.pricing .prices .try-software-button').each(function (i, e) {
                var modalText = $(this).data('modal');
                modalText = modalText.replace('one-off', 'Per Month');
                $(this).data('modal', modalText);
            });
            $('.footer-price-container .price').html('£899');
            $('.footer-price-container .try-software-button').each(function (i, e) {
                var modalText = $(this).data('modal');
                modalText = modalText.replace('Per Month', 'Buy Out');
                $(this).data('modal', modalText);
            });
            $('.price-container #price').html('£899');
            $('.price-container #software-price').html('£250');
            $('.try-software-button.buy-now-button').data('modal', 'Buy Now - Buy Out');
        }
    });
    $('.get-quote').on('click', function (e) {
        $('.modal .title').html($(this).data('modal')).show();
        openModal('#getQuote');
    });
    $('.try-software-button').on('click', function (e) {
        e.preventDefault();
        $('#tryModal .title').html($(this).data('modal')).show();
        openModal('#tryModal');
    });
    $('.sign-in-button').on('click', function () {
        openModal('#signinModal');
    });
    $(window).on('click', function (event) {
        if ($(event.target).hasClass('modal')) {
            closeModal();
        }
    });
    $('.close-modal').on('click', function () {
        closeModal();
    });
    $('.fadeInUpOnScroll').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated fadeInUp',
        offset: 100
    });
    $('.fadeInDownOnScroll').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated fadeInDown',
        offset: 100
    });
    $('.fadeInDownBigOnScroll').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated fadeInDownBig',
        offset: 100
    });
    $('.fadeInOnScroll').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated fadeIn',
        offset: 100
    });
    $('.fadeInRightOnScroll').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated fadeInRight',
        offset: 100
    });
    $('.fadeInLeftOnScroll').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated fadeInLeft',
        offset: 100
    });
    $('.bounceInOnScroll').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated bounceIn',
        offset: 100
    });
    $('.zoomInOnScroll').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated zoomIn',
        offset: 100
    });
    $('.complete-solutions').viewportChecker({
        classToAdd: 'animate-lines',
        offset: 100
    });
    $('.epos-solutions').viewportChecker({
        classToAdd: 'move-car',
        offset: 100
    });
    //menu toggle
    var scrollTop = 0;
    $('.menu-button').on('click', function () {
        var navigationMenu = $('.navigation-menu');
        navigationMenu.toggleClass('open');
        if (navigationMenu.hasClass('open')) {
            scrollTop = ($('html').scrollTop()) ? $('html').scrollTop() : $('body').scrollTop();
            $('body').addClass('no-scroll');
            setTimeout(function () {
                $('.top-section > .header').toggleClass('menu-is-open');
                $('#menu-icon').toggleClass('open');
            }, 400);
        }
        else {
            $('body').removeClass('no-scroll').scrollTop(scrollTop);
            $('.top-section > .header').toggleClass('menu-is-open');
            $('#menu-icon').toggleClass('open');
        }
    });
    //tabs
    //price tabs
    $('.price-tabs .header ul li').on('click', function () {
        $(this).siblings('li').removeClass('active');
        $(this).addClass('active');
        var index = $(this).index();
        var tabContent;
        switch (index) {
            case 0:
                tabContent = $(this).parents('.tabs').parent('.prices').children('.left');
                break;
            case 1:
                tabContent = $(this).parents('.tabs').parent('.prices').children('.center');
                break;
            case 2:
                tabContent = $(this).parents('.tabs').parent('.prices').children('.right');
                break;
        }
        $(this).parents('.tabs').parent('.prices').children('.active').removeClass('active');
        $(tabContent).addClass('active');
    });
    //not price
    var tabsHeaders = $('.tabs:not(.price-tabs) .header ul li');
    var tabsContent = tabsHeaders.parents('.tabs').children('.tabs-content-container').children('.content');
    //for mobile
    tabsHeaders.each(function (i, e) {
        $(this).append(tabsContent.children('.tab-content').eq(i).clone());
    });
    tabsHeaders.on('click', function () {
        $(this).siblings('li').removeClass('active');
        $(this).addClass('active');
        var index = $(this).index();
        $(tabsContent).children('.tab-content.active').removeClass('active');
        $(tabsContent).children('.tab-content').eq(index).addClass('active');
        $('body').animate({
            scrollTop: $(this).offset().top - 100
        }, 400);
    });
    $(window).on('resize', function () {
        setHeaderHeight();
    });
    $(window).scroll(function () {
        var topSectionBottom = $('.top-section').offset().top + $('.top-section').outerHeight(), windowScrollBottom = $(window).height() + $(this).scrollTop();
        //fix header
        var middleOfView = $(window).height() / 2;
        var header = $('.top-section > .header');
        var body = $('body');
        if ($(this).scrollTop() > middleOfView) {
            if (!body.hasClass('fixed-header')) {
                body.addClass('fixed-header');
                header.removeClass('slideOutUp').addClass('slideInDown');
            }
        }
        else {
            if ($(this).scrollTop() < middleOfView && body.hasClass('fixed-header')) {
                header.removeClass('slideInDown').addClass('slideOutUp');
                setTimeout(function () {
                    body.removeClass('fixed-header');
                    if (!$('.navigation-menu').hasClass('open')) {
                        header.removeClass('slideOutUp').addClass('fadeIn');
                    }
                }, 800);
            }
            //$('.header').removeClass('fixed');
        }
        //console.log(topSectionBottom,windowScrollBottom,footerHeight);
        var systemIntroduction = $('.system-closer-look');
        if (systemIntroduction.length) {
            if (windowScrollBottom > systemIntroduction.offset().top) {
                $('footer .main-content').removeClass('show');
                $('footer .main-content .hide-medium').removeClass('show');
                $('footer .footer-price-container').addClass('show');
            }
            else if (windowScrollBottom < systemIntroduction.offset().top) {
                $('footer .main-content').addClass('show');
                $('footer .footer-price-container').removeClass('show');
            }
        }
        var completeSystemsOrSoftwareOnly = $('.complete-systems-or-software-only');
        if (completeSystemsOrSoftwareOnly.length) {
            if (windowScrollBottom > completeSystemsOrSoftwareOnly.offset().top && windowScrollBottom != $(document).height()) {
                $('footer .main-content').removeClass('show');
                $('footer .main-content .hide-medium').removeClass('show');
                $('footer .footer-buy-now-container').addClass('show');
            }
            else if (windowScrollBottom < completeSystemsOrSoftwareOnly.offset().top || windowScrollBottom == $(document).height()) {
                $('footer .main-content').addClass('show');
                $('footer .footer-buy-now-container').removeClass('show');
            }
        }
        //parallax
        if ($('.parallax').length)
            $('.parallax > img').css('top', $('.parallax > img').offset().top - $(this).scrollTop());
    });
    //switch section
    $('.switch-sector').on('click', function () { $(this).toggleClass('open').siblings('.hide-medium').toggleClass('show'); });
    function setHeaderHeight() {
        var footerHeight = $('footer').outerHeight();
        var headerHeight = $('.header').outerHeight();
        var windowHeight = $(window).height();
        // if(windowHeight > 768){
        //$('body').css('paddingBottom',footerHeight);
        // $('.dynamic-height').css('min-height',(windowHeight - (headerHeight + footerHeight + 100)));
        // $('.introduction').css('min-height',(windowHeight - (headerHeight + footerHeight)));
        // };
    }
    function openModal(id) {
        $('.modal .modal-content').removeClass('fadeOutUp').addClass('fadeInDown');
        $(id).addClass('active');
    }
    function closeModal() {
        $('.modal .modal-content').removeClass('fadeInDown').addClass('fadeOutUp');
        setTimeout(function () {
            $('.modal').removeClass('active');
        }, 400);
    }
});
$('.video-alternative').on('load', function () {
    $(".se-pre-con").fadeOut("slow");
});
var addToBasket = function (ev, productIndex) {
    ev.preventDefault();
    var product = products[productIndex];
    var productsInOrder = localStorage["ngStorage-productsInOrder"] ? JSON.parse(localStorage["ngStorage-productsInOrder"]) : [];
    var productIndexInOrder = productsInOrder.findIndex(function (p) { return p.Id == product.Id; });
    if (productIndexInOrder != -1) {
        productsInOrder[productIndexInOrder].Quantity = parseInt(productsInOrder[productIndexInOrder].Quantity) + 1;
    }
    else {
        product.Quantity = 1;
        productsInOrder.push(product);
    }
    localStorage["ngStorage-productsInOrder"] = JSON.stringify(productsInOrder);
    localStorage["ngStorage-productsInBasket"] = localStorage["ngStorage-productsInBasket"] ? parseInt(localStorage["ngStorage-productsInBasket"]) + 1 : 1;
    $(".badge").html(localStorage["ngStorage-productsInBasket"]);
    return false;
};
$(window).on('load', function () {
    // Carousel
    // let carouselWidth = 0;
    // let carouselState = 0;
    // let slidesContainer = $('.carousel .slides');
    // let slides = $('.carousel .slides ul');
    //
    // $('.carousel .slides ul li').each(function (i,e) {
    //     carouselWidth += $(e).width();
    // });
    // slides.width(carouselWidth);
    //
    // $('.carousel .next').on('click',function () {
    //     let newLeft = slides.position().left - slidesContainer.outerWidth();
    //     carouselState++;
    //     if(slides.outerWidth() + newLeft < slidesContainer.outerWidth() ){
    //         newLeft = slidesContainer.outerWidth() - slides.outerWidth();
    //         carouselState = slides.children('li').length;
    //     }
    //
    //     slides.css('left',newLeft);
    // })
    //
    // $('.carousel .prev').on('click',function () {
    //     carouselState--;
    //     let newLeft = slides.position().left + slidesContainer.outerWidth();
    //     if(newLeft > 0 )
    //     {
    //         newLeft = 0;
    //         carouselState = 0;
    //     }
    //
    //     slides.css('left',newLeft);
    // })
    // end Carousel
    //showcase
    var thumbnailsMobile = $('#thumbnails-mobile');
    thumbnailsMobile.append($('.thumbnails').html());
    thumbnailsMobile.children('.hidden').removeClass('hidden');
    $('.showcase .thumbnail-btn').on('mouseover', function () {
        var imageSrc = $(this).children('img').attr('src');
        $(this).parents('.showcase').children('.big-image').children('img').attr('src', imageSrc);
    });
    if (thumbnailsMobile.length)
        thumbnailsMobile.slick({
            infinite: false,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4
        });
    if ($('.carousel .slides').length)
        $('.carousel .slides').slick({
            infinite: false,
            speed: 300,
            slidesToShow: 8,
            slidesToScroll: 8,
            responsive: [
                {
                    breakpoint: 1920,
                    settings: {
                        slidesToShow: 6,
                        slidesToScroll: 6
                    }
                },
                {
                    breakpoint: 1366,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5
                    }
                },
                {
                    breakpoint: 1080,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    //contact form
    var loader = "<div class=\"loading\"></div>";
    $('#contactForm').on('submit', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 400);
        $(this).hide().after(loader);
        $('#formStatus').hide().removeClass('success').removeClass('failed');
        $.ajax({
            method: "POST",
            crossDomain: true,
            url: "http://xepos.co.uk/contact.php",
            data: {
                url: window.location.href,
                name: $('#contactForm #name-field').val(),
                businessName: $('#business-name-field').val(),
                contactNumber: $('#contact-number-field').val(),
                contactEmail: $('#contact-email-field').val(),
                description: $('#description-field').val()
            }
        })
            .done(function (msg) {
            $('.loading').remove();
            $('#formStatus').show().addClass('success').html(msg);
            /* Conversion Tracking Start */
            window.google_trackConversion({
                google_conversion_id: 843085328,
                google_conversion_language: "en",
                google_conversion_format: "3",
                google_conversion_color: "ffffff",
                google_conversion_label: "BIakCJ6Z6XMQkOyBkgM",
                google_remarketing_only: false
            });
        })
            .fail(function (jqXHR, msg) {
            $('#contactForm').show();
            $('.loading').remove();
            $('#formStatus').show().addClass('failed').html("Request failed. Please try again!");
        });
    });
    //quote form
    $('#tryForm').on('submit', function (e) {
        e.preventDefault();
        $(this).hide().after(loader);
        $('.modal .title').hide();
        $('#tryformStatus').hide().removeClass('success').removeClass('failed');
        $.ajax({
            method: "POST",
            crossDomain: true,
            url: "http://xepos.co.uk/quote.php",
            data: {
                url: window.location.href,
                title: $('.modal .title').html(),
                companyName: $('#company-name-field').val(),
                name: $('#name-field').val(),
                email: $('#email-field').val(),
                tel: $('#tel-field').val()
            }
        })
            .done(function (msg) {
            $('.loading').remove();
            $('#tryformStatus').show().addClass('success').html(msg);
            window.google_trackConversion({
                google_conversion_id: 843085328,
                google_conversion_language: "en",
                google_conversion_format: "3",
                google_conversion_color: "ffffff",
                google_conversion_label: "BIakCJ6Z6XMQkOyBkgM",
                google_remarketing_only: false
            });
            /* Conversion Tracking Start */
            // let google_conversion_id = 843085328;
            // let google_conversion_language = "en";
            // let google_conversion_format = "3";
            // let google_conversion_color = "ffffff";
            // let google_conversion_label = "BIakCJ6Z6XMQkOyBkgM";
            // let google_remarketing_only = false;
            //
            // $.getScript('//www.googleadservices.com/pagead/conversion.js');
            //
            // let image = new Image(1, 1);
            // image.src = "//www.googleadservices.com/pagead/conversion/843085328/?label=BIakCJ6Z6XMQkOyBkgM&guid=ON&script=0";
        })
            .fail(function (jqXHR, msg) {
            $('#tryForm').show();
            $('.loading').remove();
            $('.modal .title').show();
            $('#tryformStatus').show().addClass('failed').html("Request failed. Please try again!");
        });
    });
});
//# sourceMappingURL=scripts.js.map