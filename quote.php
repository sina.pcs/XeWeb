<?php
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    die();
}
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: *");
$to = 'website.leads@xepos.co.uk';
$subject = $_POST['title'];
$companyName = $_POST['companyName'];
$name = $_POST['name'];
$email = $_POST['email'];
$tel = $_POST['tel'];
$url = $_POST['url'];

$htmlContent = '
    <html>
    <head>
        <title>XEPOS</title>
    </head>
    <body>
        <table cellspacing="0" style="border: 2px dashed #FB4314; width: 600px; height: 500px;">
            <tr>
                <th>Company Name:</th><td>'.$companyName.'</td>
            </tr>
            <tr style="background-color: #e0e0e0;">
                <th>Name:</th><td>'.$name.'</td>
            </tr>
            <tr>
                <th>Email:</th><td>'.$email.'</td>
            </tr>
            <tr style="background-color: #e0e0e0;">
                <th>Telephone:</th><td>'.$tel.'</td>
            </tr>
        </table>

        <div>from: '.$url.'</div>
    </body>
    </html>';

// Set content-type header for sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// Additional headers
$headers .= 'From: XEPOS <website.leads@xepos.co.uk>' . "\r\n";

// Send email
if(mail($to,$subject,$htmlContent,$headers)):
    ?>
    <div>
        Thank you for filling out your information!<br>
        We will look over your information and get back to you as soon as possible.
    </div>
    <div style="width: 125px;
    text-align: center;
    padding: 10px;
    margin: 20px auto;
    border-radius: 5px;">
        <img src="http://xepos.co.uk/assets/img/logo-white.svg" />
    </div>
    <div style="font-size: 17px;
    text-align: center;">
        <strong>Tel:</strong> <text class="number">0345 0345 930</text>
    </div>
    <?php
else:
    $errorMsg = 'Email sending fail.';
    echo $errorMsg;
endif;

$htmlContent = '
    <html>
    <head>
        <title>XEPOS</title>
    </head>
    <body>
    <div style="width:300px; margin:auto;">
        <div>
            We have received your message and would like to thank you for contacting us. If your inquiry is urgent, please use the telephone number listed below, to talk one of our staff members. Otherwise, we will reply by email shortly.
        </div>
        <div style="width: 125px;
        text-align: center;
        padding: 10px;
        margin: 20px auto;
        border-radius: 5px;">
            <img src="http://xepos.co.uk/assets/img/logo-hospitality.png" alt="Xepos-Logo" title="Xepos-Logo" style="display:block" width="120" height="30" />
        </div>
        <div style="font-size: 17px;
        text-align: center;">
            <strong>Tel:</strong> 0345 0345 930
        </div>
    </div>
    </body>
    </html>
';
mail($email,$subject,$htmlContent,$headers);
?>