<?php
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    die();
}
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: *");

$json = file_get_contents('php://input');
$input= json_decode( $json,true ); 

$to = $input['to'];
$subject = $input['subject'];
$content = $input['content'];


$htmlContent = '
    <html>
    <head>
        <title>XEPOS</title>
    </head>
    <body>
        '.$content.'
    </body>
    </html>';

// Set content-type header for sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";


// Send email
if(mail($to,$subject,$htmlContent,$headers)):
    echo $input['responseToUser'];
else:
   $errorMsg = 'Email sending fail.';
    echo $errorMsg;
endif;

$htmlContent = $input['mailToUser'];
//mail($email,$subject,$htmlContent,$headers);
?>